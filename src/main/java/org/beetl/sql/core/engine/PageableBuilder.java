package org.beetl.sql.core.engine;

/**
 * 分页实现类构建
 * create time : 2017-05-28 19:34
 *
 * @author luoyizhu@gmail.com
 */
public interface PageableBuilder {
    Pageable create();
}
